#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

const char *infile, *outfile;

void parse(FILE *in, FILE *out, int depth);
void idtof(FILE *in, FILE *out, char append);
void contof(FILE *in, FILE *out);
void eleget(FILE *in, char *buf);
void clobbr(FILE *in, char until);
void indent(FILE *out, int depth);
void fail(const char *s);


/* main
 *  `sexprxml <infile> <outfile>`
 * if outfile is not present it defaults to stdin.
 * Then make sure that input starts with a '(' for parse, and begin parsing.
 */ 
int main(int argc, char **argv)
{
	FILE *in, *out = NULL;
    if (argc<2) 
	    fail("No input!");
	infile = argv[1];
	if(!(in = fopen(infile, "r"))) 
	    fail("Caught error! (main)");
    if(argc>2) {
        outfile = argv[2];
        if(!(out = fopen(outfile, "w"))) fail("Caught error! (main)");
    }

	/* Strip out the remaining spaces */
    clobbr(in, '(');

    if(out)
        parse(in, out, 0);
    else
        parse(in, stdout, 0);
        
	exit(0);
}

/* parse
 * Recursively parse the input file using the standard recursive
 * descent model of parsing.
 */
void parse(FILE *in, FILE *out, int depth)
{
	char c, id[256];
	eleget(in, id);
    indent(out, depth);
	fprintf(out, "<%s", id);

    int ni = 0; /* 0 = indent, 1 = no indent; should be using depth for this */

	while ((c = fgetc(in)), !feof(in) && !ferror(in)) {

        if(isspace(c)) continue;
        switch(c) {
        case ';': 
            clobbr(in, '\n');
            break;
		case ':': 
		    fputc(' ', out), idtof(in, out, '='), idtof(in, out, 0); 
		    break;
		case '[': 
		    fputs(">\n", out), indent(out, depth+1), contof(in,out), ni=1;
            break;
        case '(': 
            if(!ni)
                fputc('\n', out);
            parse(in, out, depth+1);
            break;
		case ')': 
		    fputc('\n', out), indent(out, depth), fprintf(out, "</%s>", id);
            return; 
        }
        if (feof(in) || ferror(in)) 
            fail("Caught error! (parse)");
	}
}

/* idtof :: identifier to file.
 * This grabs `:key ` and `val `.
 * It basically just copies in to out until ' ' is encountered,
 * then writes append to out if it's not NULL
 */
void idtof(FILE *in, FILE *out, char append)
{
	int c;
	while ((c = fgetc(in)), !feof(in) && !ferror(in) && !isspace(c))
		fputc(c, out);
	if (feof(in) || ferror(in)) 
	    fail("Caught error! (idtof)");
	if (append > 0) 
	    fputc(append, out);
}

/* contof :: content to file
 * This grabs input from in and shoves it into out until ']' is
 * encountered. It's to grab `<content>` in `(n [content])`.
 */
void fcontent(FILE *in, FILE *out)
{
	int c;
	while ((c = fgetc(in)), !feof(in) && !ferror(in) && c != ']')
		fputc(c, out);
	if (feof(in) || ferror(in)) 
	    fail("Caught error! (contof)");
}

/* eleget :: element get
 * Grabs the input from in and shoves it into buf until ' ' is
 * encountered. It then NULL-terminates buf. It's to grab
 * `name` in `(name [test])`
 */
void gettag(FILE *in, char *buf)
{ /* buf is guaranteed to be big enough. ( 256 IS BIG ENOUGH >:( ) */
	int c;
	while((c=fgetc(in)), !feof(in) && !ferror(in) && !isspace(c)) 
		*buf++ = c;
	if(feof(in) || ferror(in)) fail("Caught error! (strget)");
	*buf = 0;
}

/* clobbr :: clobber (Hahahaha)
 * Clobbers the input until 'until' is reached.
 */
void clobbr(FILE *in, char until)
{
    while(fgetc(in)!=until)
        if(ferror(in) || feof(in)) fail("Caught error! (clobbr)");
}

/* indent :: ... indent
 * Writes depth number of indents to out
 */
void indent(FILE *out, int depth)
{
	while(depth-- > 0) 
		/* Two-char is the best <3 (I'm a lisp programmer at heart...) */
		fprintf(out, "  "); 
}

/* fail :: If you need this part then what can I say...
 * Writes error information to stdout then exits with error code 1
 */
void fail(const char *s)
{
	printf("%s", s);
    printf("\ninfile: %s\noutfile: %s\n", infile, outfile);
	perror("File errors");
	exit(1);
}